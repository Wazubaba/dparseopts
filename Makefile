# OSDev wiki for life
DC := dmd
BIN := libparseopts
INCLUDES := -Isrc

WARNINGS := -w

DFLAGS := $(WARNINGS) -g -unittest -lib

PROJDIRS := src

AUXFILES := Makefile README.md
SRCFILES := $(shell find $(PROJDIRS) -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
OBJFILES := $(patsubst %.d, %.o, $(SRCFILES))
ALLFILES := $(SRCFILES) $(AUXFILES)

.PHONY: install uninstall clean all todolist dist tests docs

all: bin/$(BIN)

bin/$(BIN): $(OBJFILES)
	@mkdir -p bin
	$(DC) -of$@ $^ $(DFLAGS) $(LIBS) -main
	ln -sf bin/$(BIN).a $(BIN).a

-include $(DEPFILES)

%.o: %.d
	$(DC) $(DFLAGS) -c $< -of$@

tmp/%.o: src/%.d $(HEADERS)
	$(DC) -c -of$@ $< $(DFLAGS)

clean:
	@-$(RM) $(wildcard $(OBJFILES) $(DEPFILES)) $(BIN).a -r release bin doc
	cd tests && $(MAKE) clean

dist: bin/$(BIN) docs
	@echo "Generating package..."
	@-mkdir -p release
	@-strip bin/$(BIN).a
	@-mkdir -p lib include
	@cp src/parseopts.d include/.
	@cp bin/$(BIN).a lib/.
#	@-upx -q --lzma --best bin/$(BIN).a
	@tar czf release/$(BIN).tar.gz $(AUXFILES) $(SRCFILES) lib include doc
	@tar cjf release/$(BIN).tar.bz2 $(AUXFILES) $(SRCFILES) lib include doc
	-@$(RM) -r lib include

tests: bin/$(BIN)
	cd tests && $(MAKE)

doc/parseopts.html:
	@mkdir -p doc
	$(DC) -main -o- -D -Dddoc/ src/parseopts.d
	-@$(RM) doc/__main.html # Idk why this happens but cba to find out why x)

docs: doc/parseopts.html

## Utility rules ##
todolist:
	-@for file in $(ALLFILES:Makefile=); do fgrep -H -e TODO -e FIXME -e XXX $$file; done; true

