import wst.cli.parseopts;

void main()
{
	string[] legalArgList = ["./test", "--rawr", "grr", "-gwr", "5"];
	Opts options = Opts(legalArgList, ["rawr", "g", "w", "r"]);
	assert(options.list.length == 4);
	assert("rawr" in options.list);
	assert("g" in options.list);
	assert("w" in options.list);
	assert("r" in options.list);
	assert(options.list["rawr"] == "grr");
	assert(options.list["g"] == "");
	assert(options.list["w"] == "");
	assert(options.list["r"] == "5");
}

