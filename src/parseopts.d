module wst.cli.parseopts;

// VERSION: 1.0.0
// Note: ^this may not catch on, or maybe it will. Idk...

/**
	Parse opts makes 5 assumptions:
	* A single option delimiter denotes a single char arg, with multiple args being permitted to be defined.
	* A double option delimiter denotes a word arg.
	* Arguments may have values or act as atoms.
	* Arguments and values are always strings.
	* Only specific arguments are desired, with invalid ones resulting
	in an error. This is optional however.
**/

private import wst.data.array: inside;

/// A malformed argument was encountered
const Exception InvalidArgument = new Exception("Invalid Argument");

/**
	Container for argument processing functionality
**/
struct Opts
{
	string[string] list;

	/**
		Helper to trigger Scan easily.
		Throws: InvalidArgument if it cannot parse the arguments

		Example:
		---
		int main(string[] args)
		{
			Opts options = Opts(args, ["help", "h"]);
			assert("help" in options.list);
			return 0;
		}
		---
	**/
	this(ref string[] args, string[] whitelist = [], bool skipFirst = true)
	{
		if (skipFirst)
			if (!scan(args[1..$], whitelist))
				throw InvalidArgument;
			else {/* Dummy because exceptions are behaving like broken goto's */}
		else
			if (!scan(args))
				throw InvalidArgument;
			else {/* Dummy because exceptions are behaving like broken goto's */}
	}

	/**
		Manually scans an array of strings as POSIX commandline. Tests against a whitelist if provided and non-empty.
		Returns: true on success or false on failure.
		Note: This function does *not* ignore the first argument!
		Example:
		---
		int main(string[] args)
		{
			Opt options;
			options.scan(["--test", "rawr"]);
			assert(options.list["test"] == "rawr");
			return 0;
		}
		---
	**/
	bool scan(string[] args, string[] whitelist = [])
	{
		string last;
		foreach (string word; args)
		{
			if (word.length > 2 && word[0..2] == "--")
			{
				if (last.length > 0)
				{
					if (whitelist.length > 0 && !last.inside(whitelist)) return false;
					list[last] = "";
					last.length = 0;
				}
				else
				{
					last = word[2..$];
				}
			} else
			if (word.length > 2 && word[0] == '-' && word[1] != '-')
			{
				if (last.length > 0)
				{
					if (whitelist.length > 0 && !last.inside(whitelist)) return false;
					list[last] = "";
					last.length = 0;
				}

				char lastChar = '\0';
				foreach (char c; word[1..$])
				{
					if (lastChar != '\0')
					{
						last.length = 1;
						last = [lastChar];
						if (whitelist.length > 0 && !last.inside(whitelist)) return false;
						list[last] = "";
						last.length = 0;
					}
					lastChar = c;
				}

				last ~= lastChar;
			}
			else
			{
				if (last.length > 0)
				{
					if (whitelist.length > 0 && !last.inside(whitelist)) return false;
					list[last] = word;
					last.length = 0;
				}
				else
				{
					return false;
				}
			}
		}
		return true;
	}
}

unittest
{
	import std.stdio: writeln;
	// rawr = grr, g and w are atoms, r = 5
	string[] legalArgList = ["./test", "--rawr", "grr", "-gwr", "5"];
	writeln("Testing with ", legalArgList, "...");
	Opts options = Opts(legalArgList, ["rawr", "g", "w", "r"]);
	assert(options.list.length == 4);
	assert("rawr" in options.list);
	assert("g" in options.list);
	assert("w" in options.list);
	assert("r" in options.list);
	assert(options.list["rawr"] == "grr");
	assert(options.list["g"] == "");
	assert(options.list["w"] == "");
	assert(options.list["r"] == "5");
}

