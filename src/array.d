module wst.data.array;

/**
	A collection of helpers for working with arrays of data
**/

/// Remove an item by `index` from `array`, returning modified `array`
auto purgeByIndex(T)(T array, size_t index)
{ // Remove specified index from any kind of database ever almost
	T swap = array[0 .. index-1];
	swap ~= array[index+1 .. $];
	return swap;
}

bool inside(T)(T needle, T[] haystack)
{
	foreach(T hay; haystack)
	{
		if (hay == needle)
			return true;
	}
	return false;
}

