# parseopts #
=============

This library serves as an alternative to optparse.
See inline docs for info, or execute `make docs`
to generate html documentation. The unittest in
`src/parseopts.d` should also give you a fairly
good idea on usage.

Enjoy o/

# UPDATE
sort of unmaintained, and dropped in favor of my new argparse module, found [here](https://gitlab.com/Wazubaba/neowst-d-argparse)